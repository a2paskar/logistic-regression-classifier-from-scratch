# Logistic Regression Classifier from scratch

Jupyter Notebook which implements the Logistic Regression algorithm from scratch for a binary classification task using linearly separable dataset.


https://github.com/cuekoo/Binary-classification-dataset for dataset.

